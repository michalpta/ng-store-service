import { Component } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs/Rx';
import { StoreService } from './store.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  item$: Observable<any>;

  constructor(private store: StoreService) {
    this.item$ = this.store.getItems();
  }
}
