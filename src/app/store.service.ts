import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/Rx';

@Injectable()
export class StoreService {

  constructor() { }

  private item$: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([
    { name: 'Item 1' },
    { name: 'Item 2' },
    { name: 'Item 3' }
  ]);

  getItems = () => this.item$.asObservable();

  updateItem(item: any) {
    const items = this.item$.getValue().map(i => {
        if (i.name === item.name) {
          i.name = item.name + ' UPDATED!';
        }
        return i;
      });
    // this.item$.next(items);
  }

}
